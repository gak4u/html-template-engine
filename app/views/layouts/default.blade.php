<!doctype html>
<html lang="en">
@include('layouts/parts/header')
<body>
	@yield('content')
	@javascripts('application')
</body>
</html>
